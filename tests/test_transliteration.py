# -*- coding: utf-8 -*-
import pytest  # noqa

from srtools.transliteration import cyrillic_to_latin
from srtools.transliteration import latin_to_cyrillic


class TestTransliteration:
    def test_cyrillic_to_latin(self):
        assert (
            cyrillic_to_latin(
                "Ђаче, уштеду плаћај жаљењем због џиновских цифара."
            )
            == "Đače, uštedu plaćaj žaljenjem zbog džinovskih cifara."
        )

        cyrillic_alphabet_lc = "абвгдђежзијклљмнњопрстћуфхцчџш"
        cyrillic_alphabet = cyrillic_alphabet_lc + cyrillic_alphabet_lc.upper()

        assert cyrillic_to_latin(cyrillic_alphabet) == (
            "abvgdđežzijklljmnnjoprstćufhcčdžš"
            "ABVGDĐEŽZIJKLLjMNNjOPRSTĆUFHCČDžŠ"
        )

    def test_latin_to_cyrillic(self):
        assert (
            latin_to_cyrillic(
                "Đače, uštedu plaćaj žaljenjem zbog džinovskih cifara."
            )
            == "Ђаче, уштеду плаћај жаљењем због џиновских цифара."
        )

        latin_alphabet_lc = "abcčćddžđefghijklljmnnjoprsštuvzž"
        latin_alphabet = latin_alphabet_lc + latin_alphabet_lc.upper()

        assert latin_to_cyrillic(latin_alphabet) == (
            "абцчћдџђефгхијклљмнњопрсштувзжАБЦЧЋДЏЂЕФГХИЈКЛЉМНЊОПРСШТУВЗЖ"
        )

    def test_latin_to_cyrillic_nonescaped_digraphs(self):
        assert latin_to_cyrillic("džDžDŽljLjLJnjNjNJ") == "џЏЏљЉЉњЊЊ"

    def test_latin_to_cyrillic_escaped_digraphs(self):
        assert (
            latin_to_cyrillic("d!žD!žD!Žl!jL!jL!Jn!jN!jN!J")
            == "джДжДЖлјЛјЛЈнјНјНЈ"
        )
