# -*- coding: utf-8 -*-
import unicodedata

import pytest  # noqa

from srtools.character_dictionaries import CYR_TO_LAT_DICT
from srtools.character_dictionaries import CYR_TO_LAT_TTABLE
from srtools.character_dictionaries import LAT_TO_CYR_DICT


def is_cyrillic(text: str) -> bool:
    return all(unicodedata.name(char).startswith("CYRILLIC") for char in text)


def is_latin(text: str) -> bool:
    return all(
        char == "!" or unicodedata.name(char).startswith("LATIN")
        for char in text
    )


class TestDicts:
    def test_cyr_to_lat_proper_characters(self):
        for cyr_letter, lat_letter in CYR_TO_LAT_DICT.items():
            assert is_cyrillic(cyr_letter)
            assert is_latin(lat_letter)

    def test_lat_to_cyr_proper_characters(self):
        for lat_letter, cyr_letter in LAT_TO_CYR_DICT.items():
            assert is_cyrillic(cyr_letter)
            assert is_latin(lat_letter)

    def test_cyr_to_lat_proper_translation_table(self):
        string_translate_table = {
            chr(cyr_letter_code): lat_letter
            for cyr_letter_code, lat_letter in CYR_TO_LAT_TTABLE.items()
        }
        assert string_translate_table == CYR_TO_LAT_DICT

    def test_cyr_to_lat_translation_table_translation(self):
        cyrillic_alphabet_lc = "абвгдђежзијклљмнњопрстћуфхцчџш"
        cyrillic_alphabet = cyrillic_alphabet_lc + cyrillic_alphabet_lc.upper()
        cyrillic_alphabet_to_latin = cyrillic_alphabet.translate(
            CYR_TO_LAT_TTABLE
        )
        assert cyrillic_alphabet_to_latin == (
            "abvgdđežzijklljmnnjoprstćufhcčdžš"
            "ABVGDĐEŽZIJKLLjMNNjOPRSTĆUFHCČDžŠ"
        )
