Changelog
================

0.1.0 (2019-11-15)
------------------

Initial version
~~~~~~~~~~~~~~~
* Command line util provides support only for UTF-8 input and output.


0.1.1 (2019-11-15)
------------------

Bug fixes
~~~~~~~~~
* Fixed some CI deployment issues.


0.1.2 (2019-11-15)
------------------

Bug fixes
~~~~~~~~~
* Documentation typo fixes.


0.1.3 (2019-11-15)
------------------

Bug fixes
~~~~~~~~~
* CI deployment fixes


0.1.4 (2019-11-17)
------------------

Bug fixes
~~~~~~~~~
* Documentation fixes

0.1.5 (2020-01-06)
------------------

Bug fixes
~~~~~~~~~
* Documentation fixes


0.1.8 (2020-01-06)
------------------

Bug fixes
~~~~~~~~~
* Documentation fixes

0.1.10 (2021-09-08)
------------------

Bug fixes
~~~~~~~~~
* Packaging fixes
