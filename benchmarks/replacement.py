#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re

from ..srtools.character_dictionaries import LAT_TO_CYR_DICT
from ..srtools.transliteration import latin_to_cyrillic
from .timer import timerfunc

test_string = (
    """\
Vuk Stefanović Karadžić je rođen 1787. godine u Tršiću blizu Loznice, u
porodici u kojoj su deca umirala, pa je po narodnom običaju, dobio ime Vuk kako
mu veštice i duhovi ne bi naudili. Njegova porodica se doselila iz Crne Gore iz
Drobnjaka. Majka Jegda, devojački Zrnić, rodom je iz Ozrinića kod Nikšića.

Pisanje i čitanje je naučio od rođaka Jevte Savića Čotrića, koji je bio jedini
pismen čovek u kraju. Obrazovanje je nastavio u školi u Loznici, ali je nije
završio zbog bolesti. Školovanje je kasnije nastavio u manastiru Tronoši. Kako
ga u manastiru nisu učili, nego terali da čuva stoku, otac ga je vratio kući.

Na početku Prvog srpskog ustanka, Vuk je bio pisar kod cerskog hajdučkog
harambaše Đorđa Ćurčije. Iste godine je otišao u Sremske Karlovce da se upiše u
gimnaziju, ali je sa 17 godina bio prestar. Jedno vreme je proveo u tamošnjoj
bogosloviji, gde je kao profesor radio Lukijan Mušicki.

Ne uspevši da se upiše u karlovačku gimnaziju, on odlazi u Petrinju, gde je
proveo nekoliko meseci učeći nemački jezik. Kasnije stiže u Beograd da upozna
Dositeja Obradovića, učenog čoveka i prosvetitelja. Vuk ga je zamolio za pomoć
kako bi nastavio sa obrazovanjem, ali ga je Dositej odbio. Vuk je razočaran
otišao u Jadar i počeo da radi kao pisar kod Jakova Nenadovića. Zajedno sa
rođakom Jevtom Savićem, koji je postao član Praviteljstvujuščeg sovjeta, Vuk je
prešao u Beograd i u Sovjetu je obavljao pisarske poslove.

Kad je Dositej otvorio Veliku školu u Beogradu, Vuk je postao njen đak. Ubrzo
je oboleo i otišao je na lečenje u Novi Sad i Peštu, ali nije uspeo da izleči
bolesnu nogu, koja je ostala zgrčena. Hrom, Vuk se 1810. vratio u Srbiju. Pošto
je kraće vreme u Beogradu radio kao učitelj u osnovnoj školi, Vuk je sa Jevtom
Savićem prešao u Negotinsku krajinu i tamo obavljao činovničke poslove.

Nakon propasti ustanka 1813. Vuk je sa porodicom prešao u Zemun, a odatle
odlazi u Beč. Tu se upoznao sa Bečlijkom Anom Marijom Kraus, kojom se oženio.
Vuk i Ana imali su mnogo dece od kojih su svi osim kćerke Mine i sina
Dimitrija, umrli u detinjstvu i ranoj mladosti (Milutin, Milica, Božidar,
Vasilija, dvoje nekrštenih, Sava, Ruža, Amalija, Aleksandrina). U Beču je
takođe upoznao cenzora Jerneja Kopitara, a povod je bio jedan Vukov spis o
propasti ustanka. Uz Kopitarevu pomoć i savete, Vuk je počeo sa sakupljanjem
narodnih pesama i sa radom na gramatici narodnog govora. Godine 1814. je u Beču
objavio zbirku narodnih pesama koju je nazvao „Mala prostonarodna
slaveno-serbska pjesnarica“. Iste godine je Vuk objavio „Pismenicu serbskoga
jezika po govoru prostoga naroda napisanu“, prvu gramatiku srpskog jezika na
narodnom govoru.

"""
    * 10
)


_LAT_TO_CYR_RX = "(" + "|".join(map(re.escape, LAT_TO_CYR_DICT.keys())) + ")"
_LAT_TO_CYR_COMP_RX = re.compile(_LAT_TO_CYR_RX, re.UNICODE | re.MULTILINE)


def _cyr_sub_string_from_lat_match(match: re.Match) -> str:
    cyr_letter = match.group()
    lat_letter = LAT_TO_CYR_DICT[cyr_letter]
    return lat_letter


@timerfunc
def f1(text):
    return _LAT_TO_CYR_COMP_RX.sub(_cyr_sub_string_from_lat_match, text)


@timerfunc
def f2(text):
    return latin_to_cyrillic(text)


if __name__ == "__main__":
    r1 = f1(test_string)
    r2 = f2(test_string)
