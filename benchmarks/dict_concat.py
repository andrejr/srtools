#!/usr/bin/env python
# -*- coding: utf-8 -*-
from functools import reduce
from itertools import chain

from .timer import timerfunc

d1 = {1: 2, 3: 4}
d2 = {5: 6, 7: 9}
d3 = {10: 8, 13: 22}


@timerfunc
def f1(*args):
    return dict(arg.items() for arg in args)


@timerfunc
def f2(*args):
    d1, d2 = args[:2]
    rest = args[2:]
    d4 = {**d1, **d2}
    for arg in rest:
        d4.update(arg)
    return d4


@timerfunc
def f3(*args):
    d4 = {}
    for d in args:
        d4.update(d)
    return d4


@timerfunc
def f4(*args):
    d4 = dict(args[0])
    for d in args[1:]:
        d4.update(d)
    return d4


@timerfunc
def f5(*args):
    return dict(chain.from_iterable(dct.items() for dct in args))


@timerfunc
def f6(*args):
    def upd(a, i):
        a.update(i)
        return a

    return reduce(upd, args, {})


if __name__ == "__main__":
    r1 = f1(d1, d2, d3)
    r2 = f2(d1, d2, d3)
    r3 = f3(d1, d2, d3)
    r4 = f4(d1, d2, d3)
    r5 = f5(d1, d2, d3)
    r6 = f6(d1, d2, d3)
