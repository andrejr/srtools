# -*- coding: utf-8 -*-
import time
from statistics import mean


def timerfunc(func):
    """
    A timer decorator
    """

    def function_timer(*args, **kwargs):
        """
        A nested function for timing other functions
        """
        runtimes = []
        for _ in range(100):
            start = time.time()
            value = func(*args, **kwargs)
            end = time.time()
            runtime = end - start
            runtimes.append(runtime)

        runtime = mean(runtimes)

        msg = "The runtime for {func} took {time} seconds to complete"
        print(msg.format(func=func.__name__, time=runtime))
        return value

    return function_timer
